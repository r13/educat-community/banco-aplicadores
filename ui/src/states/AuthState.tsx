import { createContext, ReactNode, useCallback } from "react";
import { useHistory } from "react-router";
import { IAuthState } from "../domain/auth";
import api from "../infra/axios";
import { clearToken, saveToken } from "../utils/login";

const initialState: IAuthState = {
  handleLogin: () => undefined,
  handleLogout: () => undefined,
};

export const AuthContext = createContext<IAuthState>(initialState);

type AuthStateProps = {
  children: ReactNode;
};

const AuthState = ({ children }: AuthStateProps) => {
  const history = useHistory();

  const handleLogin = useCallback(
    async (username: string, password: string) => {
      try {
        const response = await api.post("/login", {
          username,
          password,
        });

        const token = response.data.token;
        saveToken(token);
        history.push("/usuarios");
      } catch (err) {
        console.log("err", err);
      }
    },
    [history]
  );

  const handleLogout = () => {
    clearToken();
  };

  const stateValues = {
    handleLogin,
    handleLogout,
  };

  return (
    <AuthContext.Provider value={stateValues}>{children}</AuthContext.Provider>
  );
};

export default AuthState;
