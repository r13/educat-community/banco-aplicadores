import "./App.css";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import NotFoundPage from "./pages/NotFoundPage";
import AuthState from "./states/AuthState";
import UserListPage from "./pages/UserListPage";
import PrivateRoute from "./components/routes/PrivateRoute";
import UserCreatePage from "./pages/UserCreatePage";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <>
      <BrowserRouter>
        <AuthState>
          <Switch>
            <Route component={LoginPage} path="/login" />
            <PrivateRoute component={HomePage} path="/" exact />
            <Route component={NotFoundPage} path="/not-found" exact />
            <PrivateRoute component={UserListPage} path="/usuarios" exact />
            <PrivateRoute
              component={UserCreatePage}
              path="/usuarios/new"
              exact
            />
            <Redirect to="/not-found" />
          </Switch>
        </AuthState>
      </BrowserRouter>
      <ToastContainer />
    </>
  );
}

export default App;
