import { Button, Container, Typography } from "@material-ui/core";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import api from "../infra/axios";

const UserListPage = () => {
  const [users, setUsers] = useState([]);
  const history = useHistory();

  useEffect(() => {
    api
      .get("users")
      .then((r) => setUsers(r.data))
      .catch(console.log);
  }, []);

  const columns: GridColDef[] = [
    { field: "id", headerName: "ID", width: 90 },
    { field: "name", headerName: "Nome", width: 300 },
    { field: "email", headerName: "Email", width: 300 },
    { field: "date_joined", headerName: "Date de Criação", width: 300 },
  ];

  return (
    <Container>
      <Typography variant="h3" component="div" sx={{ flexGrow: 1 }}>
        Usuários
      </Typography>
      <div style={{ height: 300 }}>
        <DataGrid rows={users} columns={columns} />
      </div>
      <Button variant="contained" onClick={() => history.push("/usuarios/new")}>
        Criar
      </Button>
    </Container>
  );
};

export default UserListPage;
