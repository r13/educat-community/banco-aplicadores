import { Container, Typography } from "@material-ui/core";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import styled from "styled-components";
import UserForm from "../components/users/UserForm";
import api from "../infra/axios";

type UserCreatePageProps = {
  className?: string;
};

const UserCreatePage = ({ className }: UserCreatePageProps) => {
  const history = useHistory();

  const handleSubmit = async (data: any) => {
    try {
      await api.post("/users", data);
      history.push("/usuarios");
      toast.success("Usuário criado com sucesso.");
    } catch {
      toast.error("Ocorreu um erro ao criar um novo usuário.");
    }
  };

  return (
    <div className={className}>
      <Container>
        <Typography variant="h3" component="div" sx={{ flexGrow: 1 }}>
          Novo Usuário
        </Typography>
        <div className="form">
          <UserForm
            onSubmit={handleSubmit}
            onCancel={() => history.push("/usuarios")}
          />
        </div>
      </Container>
    </div>
  );
};

export default styled(UserCreatePage)``;
