import { ChangeEvent, useContext, useState } from "react";
import styled from "styled-components";
import { Button, TextField } from "@material-ui/core";
import { AuthContext } from "../states/AuthState";
import { Controller, useForm } from "react-hook-form";

type LoginPageProps = {
  className?: string;
};

const LoginPage = ({ className }: LoginPageProps) => {
  const { handleLogin } = useContext(AuthContext);

  const { control, handleSubmit: handleFormSubmit } = useForm({});

  const handleSubmit = async (values: any) => {
    console.log(values);
    const { username, password } = values;

    handleLogin(username, password);
  };

  return (
    <div className={className}>
      <form onSubmit={handleFormSubmit(handleSubmit)}>
        <h1>Login Page</h1>
        <Controller
          name="username"
          control={control}
          render={({ field }: { field: any }) => (
            <TextField label="Usuário" variant="standard" {...field} />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }: { field: any }) => (
            <TextField
              label="Senha"
              type="password"
              variant="standard"
              {...field}
            />
          )}
        />
        <Button type="submit" variant="contained">
          Enviar
        </Button>
      </form>
    </div>
  );
};

export default styled(LoginPage)``;
