import { TextField, Button } from "@material-ui/core";
import { Controller, useForm } from "react-hook-form";
import SelectInput from "../inputs/SelectInput";
import FileInput from "../inputs/FileInput";
import styled from "styled-components";

type UserFormProps = {
  className?: string;
  onSubmit: (data: any) => void;
  onCancel: () => void;
};

const UserForm = ({ className, onSubmit, onCancel }: UserFormProps) => {
  const { control, handleSubmit } = useForm({});

  const groupOptions = [
    {
      value: "APLICADOR",
      label: "Aplicador",
    },
    {
      value: "ADMINISTRADOR",
      label: "Administrador",
    },
    {
      value: "COORDENADOR",
      label: "Coordenador",
    },
    {
      value: "SUPORTE",
      label: "Suporte",
    },
  ];

  return (
    <form className={className} onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="username"
        control={control}
        render={({ field }: { field: any }) => (
          <TextField label="Usuário" variant="standard" {...field} />
        )}
      />
      <Controller
        name="name"
        control={control}
        render={({ field }: { field: any }) => (
          <TextField label="Nome" variant="standard" {...field} />
        )}
      />
      <Controller
        name="email"
        control={control}
        render={({ field }: { field: any }) => (
          <TextField label="Email" variant="standard" {...field} />
        )}
      />
      <Controller
        name="group"
        control={control}
        render={({ field }: { field: any }) => (
          <SelectInput
            id="group-select"
            options={groupOptions}
            label="Grupo"
            {...field}
          />
        )}
      />
      <FileInput name="photo" onUploadSuccess={(key) => console.log(key)} />
      <Button variant="outlined" onClick={onCancel}>
        Cancelar
      </Button>
      <Button type="submit" variant="contained">
        Criar
      </Button>
    </form>
  );
};

export default styled(UserForm)`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 25px;
`;
