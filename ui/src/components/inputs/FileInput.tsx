import { Button, Input } from "@material-ui/core";
import api from "../../infra/axios";

type FileInputProps = {
  name: string;
  onUploadSuccess: (key: string) => void;
};

const FileInput = ({ name, onUploadSuccess }: FileInputProps) => {
  const handleUpload = async (file: any) => {
    const formData = new FormData();
    formData.append("file", file.files[0]);

    const response = await api.post("/uploads", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });

    onUploadSuccess(response.data.key);
  };

  return (
    <label htmlFor={`${name}-button-file`}>
      <Input
        id={`${name}-button-file`}
        type="file"
        onChange={(f) => handleUpload(f.target)}
      />
      <Button variant="contained" component="span">
        Upload
      </Button>
    </label>
  );
};

export default FileInput;
