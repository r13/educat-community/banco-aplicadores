import { MenuItem, Select } from "@material-ui/core";
import { FormControl, InputLabel } from "@mui/material";

type SelectInputOption = {
  label: string;
  value: string | number;
};

type SelectInputProps = {
  id: string;
  options: SelectInputOption[];
  label: string;
};

const SelectInput = ({ id, options, label, ...props }: SelectInputProps) => {
  return (
    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
      <InputLabel id={`${id}-label`}>{label}</InputLabel>
      <Select id={id} labelId={`${id}-label`} label={label} {...props}>
        {options.map((op: SelectInputOption) => (
          <MenuItem value={op.value}>{op.label}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SelectInput;
