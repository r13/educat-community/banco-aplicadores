import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
// import MenuIcon from "@material-ui/icons";
import { Box } from "@material-ui/system";
import { useCallback } from "react";
import { useHistory } from "react-router";

type HeaderProps = {
  onLogout: () => void;
};

const Header = ({ onLogout }: HeaderProps) => {
  const history = useHistory();

  const handleLogout = useCallback(() => {
    onLogout();
    history.push("/login");
  }, [history, onLogout]);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            {/* <MenuIcon /> */}
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Banco de aplicadores
          </Typography>
          <Button variant="contained" onClick={handleLogout}>
            Sair
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
