class Carro:
    def __init__(self, modelo):
        self.modelo = modelo

    def buzinar(self):
        print('BUZINA')

    def abrir_porta_mala(self):
        print('ABRIU')

carro1 = Carro('Polo')
carro2 = Carro('Gol')

print(carro1.modelo)
print(carro2.modelo)
carro1.buzinar()
carro2.buzinar()


class Pessoa:
    def __init__(self, nome, endereco):
        self.nome = nome
        self.endereco = endereco


class PessoaFisica(Pessoa):

    def registrar_cpf(self, cpf):
        self.cpf = cpf


class PessoaJuridica(Pessoa):

    def registrar_cnpj(self, cnpj):
        self.cnpj = cnpj


pessoa1 = PessoaFisica('Lucas', 'Grao Mogol')
eduCAT = PessoaJuridica('eduCAT', 'Polos')

pessoa1.registrar_cpf('11111111111')
eduCAT.registrar_cnpj('22222222222')

print(pessoa1.nome)
print(eduCAT.nome)

print(pessoa1.cpf)
print(eduCAT.cnpj)
