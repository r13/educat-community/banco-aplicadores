
aa = 'abc'
bb = [1, 2, 3, 4, 5]
cc = range(10)


# print('string')
for a in aa:
    print(a)

# print('vetor')
for b in bb:
    print(b)

# print('range')
for c in cc:
    print(c)
