from rest_framework import routers
from users.views import UserViewSet
from django.urls import path

from rest_framework_jwt.views import obtain_jwt_token


router = routers.SimpleRouter(trailing_slash=False)

router.register('users', UserViewSet, basename='user')

urlpatterns = [
    path('login', obtain_jwt_token),
] + router.urls
