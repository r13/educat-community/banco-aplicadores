from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    name = models.CharField(max_length=1000, null=True, blank=False)
    email = models.EmailField(_('email address'), blank=False)
