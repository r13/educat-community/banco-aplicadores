import factory


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker('uuid4')
    email = factory.Faker('email')

    class Meta:
        model = 'users.User'
        django_get_or_create = ('username', )
