from rest_framework.viewsets import ModelViewSet
from users.models import User
from users.serializers import CreateUserSerializer, UserSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateUserSerializer

        return super().get_serializer_class()
