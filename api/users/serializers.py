from rest_framework import serializers
from users.models import User
from django.contrib.auth .models import Group

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'name',
            'email',
            'date_joined',
        ]

class CreateUserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    group = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = User
        fields = [
            'username',
            'name',
            'email',
            'group',
        ]

    def validate_group(self, group):
        group_obj = Group.objects.filter(name=group).first()

        if group_obj:
            return group_obj

        raise serializers.ValidationError('Group does not exists.')

    def create(self, validated_data):
        group = validated_data.pop('group', None)

        instance = super().create(validated_data)
        instance.groups.add(group)
        instance.save()

        return instance
