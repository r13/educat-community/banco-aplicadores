from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from django.contrib.auth import get_user_model
from users.factories import UserFactory

User = get_user_model()


class CreateUserTestCase(APITestCase):
    fixtures = ['groups']

    def setUp(self):
        self.user = UserFactory()
        self.client.force_authenticate(self.user)
        self.url = reverse('user-list')

    def test_create_user(self):
        payload = {
            'username': 'login1',
            'name': 'user',
            'email': 'user@email.com',
            'group': 'APLICADOR',
        }
        response = self.client.post(self.url, payload)
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        user = User.objects.get(username=payload['username'])
        self.assertEqual(user.name, payload['name'])
        self.assertEqual(user.email, payload['email'])
        self.assertTrue(user.groups.filter(name='APLICADOR').exists())

    def test_name_is_required(self):
        payload = {
            'username': 'login1',
            'email': 'user@email.com',
        }
        response = self.client.post(self.url, payload)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertIn('name', response.data.keys())

    def test_email_is_required(self):
        payload = {
            'username': 'login1',
            'name': 'user',
        }
        response = self.client.post(self.url, payload)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertIn('email', response.data.keys())

    def test_group_validation_check_group_exists(self):
        payload = {
            'username': 'login1',
            'name': 'user',
            'email': 'user@email.com',
            'group': 'NAO_EXISTE',
        }
        response = self.client.post(self.url, payload)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertIn('group', response.data.keys())
