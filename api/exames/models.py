from django.db import models

from helpers.models import BaseModel


class Exame(BaseModel):
    nome = models.CharField(max_length=5000)
    inicio = models.DateTimeField()
    termino = models.DateTimeField()
    cliente = models.ForeignKey('clientes.Cliente', on_delete=models.PROTECT)
