from django.contrib import admin
from exames.models import Exame


@admin.register(Exame)
class ExameAdmin(admin.ModelAdmin):
    search_fields = ['nome', 'cliente__nome']
    list_display = ['nome', 'cliente']
    list_filter = ('cliente__nome', 'nome')
    autocomplete_fields = ['cliente']
