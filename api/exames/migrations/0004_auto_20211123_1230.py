# Generated by Django 3.0.14 on 2021-11-23 12:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exames', '0003_auto_20211123_1229'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exame',
            old_name='autualizado_em',
            new_name='atualizado_em',
        ),
    ]
