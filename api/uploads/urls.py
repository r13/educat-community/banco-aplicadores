from rest_framework import routers
from uploads.views import UploadView
from django.urls import path

urlpatterns = [
    path('uploads', UploadView.as_view()),
]
