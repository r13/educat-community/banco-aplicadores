from rest_framework.response import Response
from rest_framework.views import APIView

from helpers.s3 import upload_file
from uuid import uuid4

class UploadView(APIView):

    def post(self, request):
        file_obj = request.FILES['file']
        key = f'banco-aplicadores/{str(uuid4())}'
        upload_result = upload_file(file_obj, key)

        return Response(
            {
                'status': upload_result,
                'key': key
            }
        )
