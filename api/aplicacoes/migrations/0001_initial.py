# Generated by Django 3.0.14 on 2021-11-23 12:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('exames', '0004_auto_20211123_1230'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aplicacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('criado_em', models.DateTimeField(auto_now_add=True)),
                ('atualizado_em', models.DateTimeField(auto_now=True)),
                ('sala', models.IntegerField()),
                ('nota', models.IntegerField(blank=True, null=True)),
                ('observacao', models.TextField()),
                ('exame', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='exames.Exame')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
