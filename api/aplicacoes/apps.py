from django.apps import AppConfig


class AplicacoesConfig(AppConfig):
    name = 'aplicacoes'
