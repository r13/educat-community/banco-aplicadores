from django.db import models
from helpers.models import BaseModel
from django.contrib.auth import get_user_model


User = get_user_model()

class Aplicacao(BaseModel):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    exame = models.ForeignKey('exames.Exame', on_delete=models.PROTECT)
    sala = models.IntegerField()
    nota = models.IntegerField(null=True, blank=True)
    observacao = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return f'{self.user.last_name} - {self.exame}'

    class Meta:
        verbose_name = 'Aplicação'
        verbose_name_plural = 'Aplicações'
