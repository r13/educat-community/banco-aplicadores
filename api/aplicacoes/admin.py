from django.contrib import admin
from aplicacoes.models import Aplicacao


@admin.register(Aplicacao)
class AplicacaoAdmin(admin.ModelAdmin):
    autocomplete_fields = ['user', 'exame']
    list_display = ['user', 'exame', 'sala']
