from django.db import models


class BaseModel(models.Model):
    criado_em = models.DateTimeField(auto_now_add=True)
    atualizado_em = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        if hasattr(self, 'nome'):
            return self.nome

        return super().__str__()

    class Meta:
        abstract = True
