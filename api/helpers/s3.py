import boto3
from botocore.exceptions import ClientError
from django.conf import settings



def upload_file(file_obj, key):
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_fileobj(file_obj, settings.BUCKET_NAME, key)
    except ClientError as e:
        return False
    return True
