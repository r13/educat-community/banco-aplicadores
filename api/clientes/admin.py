from django.contrib import admin
from clientes.models import Cliente


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    search_fields = ['nome']
    list_display = ['nome', 'endpoint']
