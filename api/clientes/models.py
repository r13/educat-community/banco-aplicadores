from django.db import models

from helpers.models import BaseModel


class Cliente(BaseModel):
    nome = models.CharField(max_length=500)
    endpoint = models.CharField(max_length=500)
