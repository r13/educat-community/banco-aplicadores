// map
console.log("MAP");
const vetor = [1, 2, 3];
const vetor2 = vetor.map((value) => value * 2);
const vetor3 = vetor.map((value) => ({
  name: value,
  label: value,
}));
console.log(vetor);
console.log(vetor2);
console.log(vetor3);

// filter
console.log("filter");
const vetorF = [1, 2, 3];
const vetorF2 = vetor.filter((value) => value < 2);
const vetorF3 = vetor.filter((value) => value < 3);
console.log(vetorF);
console.log(vetorF2);
console.log(vetorF3);

// forEach

// reduce
const vetorR = [1, 2, 3];
const vetor1R = vetorR.reduce((previous, current) => previous + current, 0);
const vetor2R = vetorR.reduce((previous, current) => previous + current, 10);
const vetor3R = vetorR.reduce((previous, current) => previous * current, 1);
console.log(vetor1R);
console.log(vetor2R);
console.log(vetor3R);
