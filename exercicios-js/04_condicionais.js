if (1 > 2) {
  console.log("nunca vai ser printado");
} else {
  console.log("vai ser printado.");
}

if (2 > 1) {
  console.log("vai ser printado.");
}

const A = "3";

switch (A) {
  case 3:
    console.log(3);
  case "3":
    console.log("33");
    break;
  case "31":
    console.log("31");
  default:
    console.log("A");
}
