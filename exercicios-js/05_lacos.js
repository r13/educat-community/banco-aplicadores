function oi() {
  console.log("oi");
}

const oi2 = () => {
  console.log("oi2");
};

function chamafor(ele) {
  console.log(ele);
}

const vet = [1, 2, 3];

oi2();
oi();

for (var i = 0; i < 10; i++) {
  console.log(i);
}

vet.forEach((ele) => console.log(ele));
vet.forEach(chamafor);
